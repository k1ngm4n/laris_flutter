import 'package:flutter/material.dart';

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

class AppColors {
  static Color PRIMARY_COLOR = HexColor("466884");
  static Color PRIMARY_COLOR_LIGHT = HexColor("7496b4");
  static Color PRIMARY_COLOR_DARK = HexColor("173e57");
  static Color ACCENT_COLOR = HexColor("FF4081");
  
  static Color WHITE_LIGHT = HexColor("f9f9f9");
  static Color BLUE_LIGHT = HexColor("dce9ef");

  static Color YELLOW_GOLD = HexColor("f1c40f");
  static Color YELLOW_LIGHT = HexColor("f3f4b5");

  static Color GREY_LIGHT = HexColor("eff1f2");

  static Color GREY = HexColor("cccfd1");
}