
class Result {
  bool mStatus = false;
  String mMessage = "";
  String mErrorCode = "";
}

class OrderDetail {
  String menuName = "";
  double mQty = 0.0;
  int mStatus = 0;
  double mPrice = 0.0;
  double mTotal = 0.0;
}