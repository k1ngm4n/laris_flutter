import 'package:flutter/material.dart';
import 'DataClass.dart';

class WebDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return myDialog();
  }
}

class myDialog extends StatefulWidget {
  @override
  _myDialogState createState() => _myDialogState();
}

class _myDialogState extends State<myDialog> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {},
      child: Dialog(
        backgroundColor: Colors.white,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
        child: Container(
          height: 300,
          width: 300,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircularProgressIndicator(),
              RaisedButton(
                onPressed: () {
                  Result mResult = new Result();
                  mResult.mStatus = true;
                  mResult.mMessage = "Sukses Man";

                  Navigator.pop(context, mResult);
                },
                child: Text("aaa"),
                color: Colors.green,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
