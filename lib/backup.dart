import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app2/resources/app_colors.dart';
import 'package:marquee/marquee.dart';

void main() => runApp(OrderRoute());

var myTheme = ThemeData(
    canvasColor: AppColors.BLUE_LIGHT,
    brightness: Brightness.dark,
    primaryColor: AppColors.PRIMARY_COLOR,
    primaryColorLight: AppColors.PRIMARY_COLOR_LIGHT,
    primaryColorDark: AppColors.PRIMARY_COLOR_DARK,
    accentColor: AppColors.ACCENT_COLOR);

class OrderRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: myTheme,
      debugShowCheckedModeBanner: false,
      home: orderPage(),
    );
  }
}

class orderPage extends StatefulWidget {
  @override
  _orderPageState createState() {
    return _orderPageState();
  }
}

class _orderPageState extends State<orderPage> {
  Widget myDrawer() {
    return SafeArea(
      child: Drawer(
        child: Column(
          children: <Widget>[
            Container(
              color: AppColors.PRIMARY_COLOR_LIGHT,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 10,
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.store,
                      size: 35,
                    ),
                    title: Text("Demo Store", style: TextStyle(fontWeight: FontWeight.w400, fontSize: 18)),
                  ),
                  ListTile(
                    leading: Icon(
                      Icons.person,
                      size: 35,
                    ),
                    title: Text("Demo Account", style: TextStyle(fontWeight: FontWeight.w400, fontSize: 18)),
                    subtitle: Text("demo"),
                  ),
                  Divider(
                    color: Colors.white,
                    thickness: 1,
                  ),
                  Align(
                      alignment: Alignment.center,
                      child: Padding(
                          padding: EdgeInsets.only(top: 3, bottom: 10),
                          child: Text("Work Date : Monday, 10 Feb 2020"))),
                ],
              ),
            ),
            Container(
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: Icon(
                      Icons.calendar_today,
                      size: 24,
                      color: Colors.black,
                    ),
                    title: Text("Stop Work Date", style: TextStyle(color: Colors.black)),
                  ),
                  Divider(
                    color: Colors.grey,
                  ),
                  ListTile(
                    dense: true,
                    leading: Icon(
                      Icons.sync,
                      size: 24,
                      color: Colors.black,
                    ),
                    title: Text("Sync Master Data", style: TextStyle(color: Colors.black)),
                  ),
                  Divider(
                    color: Colors.grey,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget myAppBar() {
    return PreferredSize(
      preferredSize: Size.fromHeight(50),
      child: AppBar(
        actions: <Widget>[
          FlatButton.icon(
              onPressed: () {},
              icon: Image.asset(
                'assets/images/ic_new_bill.png',
                height: 25,
                width: 25,
              ),
              label: Text("New Bill")),
          FlatButton.icon(onPressed: () {}, icon: Icon(Icons.content_copy), label: Text("Active Bill")),
          FlatButton.icon(
              onPressed: () {},
              icon: Image.asset(
                'assets/images/ic_table.png',
                height: 25,
                width: 25,
              ),
              label: Text("Table List")),
        ],
      ),
    );
  }

  Widget body_header_info() {
    return Material(
      color: AppColors.PRIMARY_COLOR,
      child: InkWell(
        onTap: () {
          print("AAAA");
        },
        splashColor: AppColors.PRIMARY_COLOR_LIGHT,
        child: Container(
          child: Column(
            children: <Widget>[
              SizedBox(height: 8),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    'assets/images/ic_new_bill.png',
                    height: 24,
                    width: 24,
                  ),
                  SizedBox(width: 5),
                  Text(
                    "New Bill",
                    style: TextStyle(fontSize: 16),
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[SizedBox(width: 5), Icon(Icons.person), SizedBox(width: 5), Text("Umum")],
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Icon(Icons.assignment),
                        SizedBox(width: 5),
                        Text("Dine in - 08"),
                        SizedBox(width: 5),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
            ],
          ),
        ),
      ),
    );
  }

  Widget myBody() {
    return Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
      Divider(
        height: 2,
        color: Colors.white,
      ),
      body_workdate_warning(),
      body_header_info(),
      Flexible(
        child: Container(
          color: Colors.green,
        ),
      ),
      body_action_function(),
      Divider(
        height: 1,
        color: Colors.white,
      ),
      body_action_save(),
    ]);
  }

  Color getColor(String mTag) {
    if (_ACTION_ACT == "DOWN") {
      if (mTag == _ACTION_TAG) {
        return AppColors.YELLOW_GOLD;
      }
    } else {
      if (mTag == _ACTION_TAG) {
        return Colors.white;
      }
    }
  }

  String _ACTION_TAG = "";
  String _ACTION_ACT = "";
  Widget function_icon_NEW(IconData mIcon, String mText, VoidCallback mAction, String mTag) {
    return SizedBox(
      width: 60,
      child: GestureDetector(
        onTapUp: (details) {
          _ACTION_TAG = mTag;
          _ACTION_ACT = "UP";

          print(_ACTION_ACT);

          setState(() {});
        },
//        onTapCancel: () {
//          _ACTION_TAG = mTag;
//          _ACTION_ACT = "UP";
//
//          setState(() {});
//        },
        onTapDown: (details) {
          _ACTION_TAG = mTag;
          _ACTION_ACT = "DOWN";

          print(_ACTION_ACT);

          setState(() {});
        },
        child: Material(
          color: AppColors.PRIMARY_COLOR,
          child: Column(
            children: <Widget>[
              Icon(
                mIcon,
                size: 35,
                color: getColor(mTag),
              ),
              Text(
                mText,
                style: TextStyle(color: getColor(mTag)),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget function_icon(IconData mIcon, String mText, VoidCallback mAction, String mTag) {
    return Expanded(
      flex: 1,
      child: Material(
        color: AppColors.PRIMARY_COLOR,
        child: GestureDetector(
          onTapUp: (TapUpDetails details) {
            _ACTION_TAG = mTag;
            _ACTION_ACT = "UP";

            setState(() {});
          },

          onTapDown: (TapDownDetails details) {
            _ACTION_TAG = mTag;
            _ACTION_ACT = "DOWN";

            setState(() {});
          },

          child: Column(
            children: <Widget>[
              Icon(
                mIcon,
                size: 35,
                color: getColor(mTag),
              ),
              Text(mText, style: TextStyle(color: getColor(mTag)),)
            ],
          ),
        ),
      ),
    );
  }

  Widget body_action_function() {
    return Container(
      color: AppColors.PRIMARY_COLOR,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 4),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            function_icon_NEW(Icons.print, "Print", () {}, "PRT"),
            function_icon_NEW(Icons.print, "Print2", () {}, "PRT2"),
            function_icon_NEW(Icons.print, "Print3", () {}, "PRT3"),


          ],
        ),
      ),
    );
  }

  Widget body_action_save() {
    return Visibility(
      visible: true,
      child: Container(
        height: 60,
        color: AppColors.PRIMARY_COLOR,
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: FlatButton(
                onPressed: () {},
                child: Text(
                  "SAVE",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w300),
                ),
              ),
            ),
            Container(
              width: 1,
              color: Colors.white,
            ),
            Expanded(
              flex: 2,
              child: Container(
                color: AppColors.PRIMARY_COLOR_DARK,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                      onPressed: () {},
                      child: Text(
                        "PAY NOW",
                        style: TextStyle(fontSize: 30, fontWeight: FontWeight.w300),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget body_workdate_warning() {
    return Visibility(
      visible: true,
      child: Container(
        color: AppColors.YELLOW_LIGHT,
        height: 20,
        child: Marquee(
          crossAxisAlignment: CrossAxisAlignment.center,
          text: "Work Date : Monday, 12 Feb 2020",
          style: TextStyle(color: Colors.black),
          blankSpace: 100,
          accelerationDuration: Duration(seconds: 1),
          accelerationCurve: Curves.linear,
        ),
      ),
    );
  }

  Widget actionIcon(String mTitle, AssetImage mIcon, String mTag) {
    return InkWell(
      onTap: () {},
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              ImageIcon(
                mIcon,
                size: 20,
              ),
              Text(
                " $mTitle",
                style: TextStyle(fontSize: 14),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget actionTitle() {
    return Material(
      color: AppColors.PRIMARY_COLOR,
      child: Container(
        height: 45,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(flex: 1, child: actionIcon("New Bill", AssetImage('assets/images/ic_new_bill.png'), "NewBill")),
            Expanded(
                flex: 1,
                child: actionIcon("Active Bill", AssetImage('assets/images/ic_content_copy.png'), "ActiveBill")),
            Expanded(flex: 1, child: actionIcon("Table List", AssetImage('assets/images/ic_table.png'), "TableList")),
          ],
        ),
      ),
    );
  }


  Widget myAppBar_NEW(GlobalKey<ScaffoldState> drawerKey) {
    return PreferredSize(
      preferredSize: Size.fromHeight(45),
      child: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () {
            drawerKey.currentState.openDrawer();
          },
          icon: ImageIcon(
            AssetImage('assets/images/ic_hamburger.png'),
            size: 30,
          ),
        ),
        titleSpacing: 0,
        title: actionTitle(),
      ),
    );
  }

  GlobalKey<ScaffoldState> drawerKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: myAppBar_NEW(drawerKey),
      drawer: myDrawer(),
      body: myBody(),
    );
  }
}
