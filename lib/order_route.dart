import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

//import 'package:flutter/widgets.dart';
import 'package:flutter_app2/DataClass.dart';
import 'package:flutter_app2/resources/app_colors.dart';

//import 'package:flutter_widgets/flutter_widgets.dart';
import 'package:marquee/marquee.dart';
import 'package:random_string/random_string.dart';

void main() => runApp(OrderRoute());
//final ThemeData theme = ThemeData();


// var myTheme = ThemeData(
//     canvasColor: AppColors.BLUE_LIGHT,
//     brightness: Brightness.dark,
//     primaryColor: AppColors.PRIMARY_COLOR,
//     primaryColorLight: AppColors.PRIMARY_COLOR_LIGHT,
//     primaryColorDark: AppColors.PRIMARY_COLOR_DARK, colorScheme: theme.colorScheme.copyWith(secondary: AppColors.ACCENT_COLOR));

class OrderRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //theme: myTheme,
      debugShowCheckedModeBanner: false,
      home: OrderPage(),
    );
  }
}

class OrderPage extends StatefulWidget {
  @override
  _OrderPageState createState() {
    return _OrderPageState();
  }
}

List<OrderDetail> getFakeOrder() {
  List<OrderDetail> mResult = <OrderDetail>[];

  OrderDetail temp = new OrderDetail();
  temp.menuName = "";
  temp.mPrice = 0;
  temp.mQty = 0;
  temp.mTotal = temp.mPrice * temp.mQty;
  mResult.add(temp);

  temp.menuName = "Nasi Goreng Gila";
  temp.mPrice = 20000;
  temp.mQty = 3;
  temp.mTotal = temp.mPrice * temp.mQty;
  mResult.add(temp);

  temp = new OrderDetail();
  temp.menuName = "Kwetiau Sapi";
  temp.mPrice = 40000;
  temp.mQty = 1;
  temp.mTotal = temp.mPrice * temp.mQty;
  mResult.add(temp);

  temp = new OrderDetail();
  temp.menuName = "Teh Botol";
  temp.mPrice = 5000;
  temp.mQty = 4;
  temp.mTotal = temp.mPrice * temp.mQty;
  mResult.add(temp);

  return mResult;
}

class _OrderPageState extends State<OrderPage> {
  GlobalKey<ScaffoldState> drawerKey = GlobalKey();
  String currentActionTag = "";
  String currentActionAct = "";

  int currentRowIndex = -1;
  bool currentRowShowDetail = false;
  bool isFirstRowDetailClick = false;

  var mOrderList = getFakeOrder();

  //ItemScrollController? listviewController = ItemScrollController();

  ScrollController scrollController = new ScrollController();

  Widget myDrawer() {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            color: AppColors.PRIMARY_COLOR_LIGHT,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                ListTile(
                  leading: Icon(
                    Icons.store,
                    size: 35,
                  ),
                  title: Text("Demo Store",
                      style:
                          TextStyle(fontWeight: FontWeight.w400, fontSize: 18)),
                ),
                ListTile(
                  leading: Icon(
                    Icons.person,
                    size: 35,
                  ),
                  title: Text("Demo Account",
                      style:
                          TextStyle(fontWeight: FontWeight.w400, fontSize: 18)),
                  subtitle: Text("demo"),
                ),
                Divider(
                  color: Colors.white,
                  thickness: 1,
                ),
                Align(
                    alignment: Alignment.center,
                    child: Padding(
                        padding: EdgeInsets.only(top: 3, bottom: 10),
                        child: Text("Work Date : Monday, 10 Feb 2020"))),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                ListTile(
                  leading: Icon(
                    Icons.calendar_today,
                    size: 24,
                    color: Colors.black,
                  ),
                  title: Text("Stop Work Date",
                      style: TextStyle(color: Colors.black)),
                ),
                Divider(
                  color: Colors.grey,
                ),
                ListTile(
                  dense: true,
                  leading: Icon(
                    Icons.sync,
                    size: 24,
                    color: Colors.black,
                  ),
                  title: Text("Sync Master Data",
                      style: TextStyle(color: Colors.black)),
                ),
                Divider(
                  color: Colors.grey,
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget myAppBar(GlobalKey<ScaffoldState> drawerKey) {
    Widget actionTitle() {
      Widget actionIcon(String mTitle, AssetImage mIcon, String mTag) {
        return InkWell(
          onTap: () {},
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  ImageIcon(
                    mIcon,
                    size: 20,
                  ),
                  Text(
                    " $mTitle",
                    style: TextStyle(fontSize: 14),
                  )
                ],
              ),
            ],
          ),
        );
      }

      return Material(
        color: AppColors.PRIMARY_COLOR,
        child: Container(
          height: 45,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: actionIcon("New Bill",
                      AssetImage('assets/images/ic_new_bill.png'), "NewBill")),
              Expanded(
                  flex: 1,
                  child: actionIcon(
                      "Active Bill",
                      AssetImage('assets/images/ic_content_copy.png'),
                      "ActiveBill")),
              Expanded(
                  flex: 1,
                  child: actionIcon("Table List",
                      AssetImage('assets/images/ic_table.png'), "TableList")),
            ],
          ),
        ),
      );
    }

    return PreferredSize(
      preferredSize: Size.fromHeight(45),
      child: AppBar(
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () {
            drawerKey.currentState?.openDrawer();
          },
          icon: ImageIcon(
            AssetImage('assets/images/ic_hamburger.png'),
            size: 30,
          ),
        ),
        titleSpacing: 0,
        title: actionTitle(),
      ),
    );
  }

  Widget myBody() {
    Widget bodyWordateWarning() {
      return Visibility(
        visible: true,
        child: Container(
          color: AppColors.YELLOW_LIGHT,
          height: 20,
          child: Marquee(
            crossAxisAlignment: CrossAxisAlignment.center,
            text: "Work Date : Monday, 12 Feb 2020",
            style: TextStyle(color: Colors.black),
            blankSpace: 100,
            accelerationDuration: Duration(seconds: 1),
            accelerationCurve: Curves.linear,
          ),
        ),
      );
    }

    Widget bodyHeaderInfo() {
      return Material(
        color: AppColors.PRIMARY_COLOR,
        child: InkWell(
          onTap: () {
            print("AAAA");
          },
          splashColor: AppColors.PRIMARY_COLOR_LIGHT,
          child: Container(
            child: Column(
              children: <Widget>[
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      'assets/images/ic_new_bill.png',
                      height: 20,
                      width: 20,
                    ),
                    SizedBox(width: 5),
                    Text(
                      "New Bill",
                      style: TextStyle(fontSize: 16),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(width: 5),
                          Icon(
                            Icons.person,
                            size: 20,
                          ),
                          SizedBox(width: 5),
                          Text("Umum")
                        ],
                      ),
                    ),
                    Expanded(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Icon(Icons.assignment, size: 20),
                          SizedBox(width: 5),
                          Text("Dine in - 08"),
                          SizedBox(width: 5),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 5),
              ],
            ),
          ),
        ),
      );
    }

    Widget bodyActionFunction() {
      Widget functionIcon(
          IconData mIcon, String mText, VoidCallback mAction, String mTag) {
        Color getColor(String mTag) {
          Color result = Colors.white;
          if (currentActionAct == "DOWN") {
            if (mTag == currentActionTag) {
              result = AppColors.YELLOW_GOLD;
            }
          } else {
            if (mTag == currentActionTag) {
              result = Colors.white;
            }
          }
          return result;
        }

        return Expanded(
          flex: 1,
          child: Material(
            color: AppColors.PRIMARY_COLOR,
            child: GestureDetector(
              onTapUp: (TapUpDetails details) {
                currentActionTag = mTag;
                currentActionAct = "UP";

                setState(() {});
              },
              onTapDown: (TapDownDetails details) {
                currentActionTag = mTag;
                currentActionAct = "DOWN";

                setState(() {});
              },
              child: Column(
                children: <Widget>[
                  Icon(
                    mIcon,
                    size: 35,
                    color: getColor(mTag),
                  ),
                  Text(
                    mText,
                    style: TextStyle(color: getColor(mTag)),
                  )
                ],
              ),
            ),
          ),
        );
      }

      return Container(
        color: AppColors.PRIMARY_COLOR,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 4),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              functionIcon(Icons.print, "Print", () {}, "PRT"),
              functionIcon(Icons.print, "Print2", () {}, "PRT2"),
              functionIcon(Icons.print, "Print3", () {}, "PRT3"),
            ],
          ),
        ),
      );
    }

    Widget bodyActionSave() {
      return Visibility(
        visible: true,
        child: Container(
          color: AppColors.PRIMARY_COLOR,
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: SizedBox(
                  height: 60,
                  child: FlatButton(
                    onPressed: () {},
                    child: Text(
                      "SAVE",
                      style: TextStyle(fontSize: 30, fontWeight: FontWeight.w300),
                    ),
                  ),
                ),
              ),
              Container(
                width: 1,
                color: Colors.white,
              ),
              Expanded(
                flex: 2,
                child: Container(
                  color: AppColors.PRIMARY_COLOR_DARK,
                  child: SizedBox(
                    height: 60,
                    child: FlatButton(
                      onPressed: () {},
                      child: Text(
                        "PAY NOW",
                        style: TextStyle(fontSize: 30, fontWeight: FontWeight.w300),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      );
    }

    Widget bodyOrderList() {
      Widget orderListHeader() {
        return Column(
          children: <Widget>[
            Container(
              height: 30,
              color: AppColors.GREY,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                    width: 60,
                    child: Text("Qty",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold)),
                  ),
                  SizedBox(width: 5),
                  Expanded(
                    flex: 1,
                    child: Text("Item",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold)),
                  ),
                  Visibility(
                      visible: false,
                      child: Icon(Icons.check, color: Colors.green, size: 25)),
                  Container(
                    width: 90,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 8),
                      child: Text(
                        "Total",
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        );
      }

      Color getColorRow(int index) {
        print("index : $index -> curr : $currentRowIndex");

        if (index == currentRowIndex) {
          return AppColors.YELLOW_LIGHT;
        }

        Color result = AppColors.WHITE_LIGHT;
        if (index.isOdd) {
          result = AppColors.GREY_LIGHT;
        } else {
          result = AppColors.WHITE_LIGHT;
        }
        return result;
      }

      bool getDetailRowVisibility(int index) {
        if (currentRowIndex == index) {
          if (isFirstRowDetailClick) {
            return true;
          } else {
            return currentRowShowDetail;
          }
        } else {
          return false;
        }
      }

      Widget oneOrderList(int index, OrderDetail mData) {
        return GestureDetector(
          onTap: () {
            setState(() {
              if (currentRowIndex == index) {
                currentRowShowDetail = !currentRowShowDetail;
                isFirstRowDetailClick = false;
              } else {
                currentRowShowDetail = true;
                isFirstRowDetailClick = true;
              }

              currentRowIndex = index;
              //listviewController.scrollTo(index: currentRowIndex, duration: Duration(seconds: 1));
            });
          },
          child: Material(
            color: getColorRow(index),
            child: Column(
              children: <Widget>[
                //row utama
                Container(
                  height: 30,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Container(
                        width: 60,
                        child: Text(mData.mQty.toString(),
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.black)),
                      ),
                      SizedBox(width: 5),
                      Expanded(
                        flex: 1,
                        child: Text(
                          mData.menuName,
                          style: TextStyle(color: Colors.black),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Visibility(
                          visible: false,
                          child:
                              Icon(Icons.check, color: Colors.green, size: 25)),
                      Container(
                        width: 90,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 8),
                          child: Text(
                            mData.mTotal.toString(),
                            textAlign: TextAlign.right,
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                Divider(height: 1, color: Colors.grey),

                //row modify
                Visibility(
                  visible: getDetailRowVisibility(index),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          FlatButton.icon(
                              onPressed: () {
                                setState(() {
                                  mOrderList.removeAt(index);
                                  currentRowIndex = -1;
                                });
                              },
                              icon: Icon(
                                Icons.delete,
                                color: Colors.black,
                                size: 18,
                              ),
                              label: Text(
                                "Delete",
                                style: TextStyle(color: Colors.black, fontWeight: FontWeight.w400),
                              )),
                          FlatButton.icon(
                              onPressed: () {
                                print("aaa");
                              },
                              icon: Icon(
                                Icons.edit,
                                color: Colors.black,
                                size: 18,
                              ),
                              label: Text(
                                "Modify",
                                style: TextStyle(color: Colors.black, fontWeight: FontWeight.w400),
                              )),
                          IconButton(
                            onPressed: () {
                              setState(() {
                                mData.mQty++;
                              });
                            },
                            iconSize: 35,
                            icon: Icon(
                              Icons.add_circle_outline,
                              color: Colors.black,
                            ),
                          ),
                          IconButton(
                            onPressed: () {
                              setState(() {
                                mData.mQty--;
                              });
                            },
                            iconSize: 35,
                            icon: Icon(
                              Icons.remove_circle_outline,
                              color: Colors.black,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      }

      Widget fab() {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            width: 60,
            height: 60,
            child: FloatingActionButton(
              backgroundColor: AppColors.PRIMARY_COLOR_LIGHT,
              child: Icon(
                Icons.add,
                color: Colors.white,
                size: 60,
              ),
              onPressed: () {
                setState(() {
                  OrderDetail temp = new OrderDetail();
                  temp.menuName = randomString(10);
                  temp.mPrice = double.parse(randomNumeric(2));
                  temp.mQty = double.parse(randomNumeric(2));
                  temp.mTotal = temp.mPrice * temp.mQty;
                  mOrderList.add(temp);

                  isFirstRowDetailClick = false;
                  currentRowShowDetail = false;
                  currentRowIndex = mOrderList.length - 1;

                  //scrollController.animateTo(100.0, duration: Duration(milliseconds: 500), curve: Curves.easeOut);
                  scrollController
                      .jumpTo(scrollController.position.maxScrollExtent);

                  //listviewController.jumpTo(index: currentRowIndex);
                });
              },
            ),
          ),
        );
      }

      return Expanded(
          child: Container(
        color: Colors.white,
        child: Stack(
          children: [
//            ScrollablePositionedList.builder(
//              itemScrollController: listviewController,
//              itemBuilder: (BuildContext context, int index) {
//                if (index == 0) {
//                  return orderListHeader();
//                } else {
//                  return oneOrderList(index, mOrderList[index]);
//                }
//              },
//              itemCount: mOrderList.length,
//            ),

            ListView.builder(
              controller: scrollController,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                if (index == 0) {
                  return orderListHeader();
                } else {
                  return oneOrderList(index, mOrderList[index]);
                }
              },
              itemCount: mOrderList.length,
            ),

            Align(alignment: Alignment.bottomRight, child: fab()),
          ],
        ),
      ));
    }

    return Column(mainAxisSize: MainAxisSize.max, children: <Widget>[
      Divider(height: 2, color: Colors.white),
      bodyWordateWarning(),
      bodyHeaderInfo(),
      bodyOrderList(),
      bodyActionFunction(),
      Divider(height: 1, color: Colors.white),
      bodyActionSave(),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: drawerKey,
        drawer: myDrawer(),
        appBar: myAppBar(drawerKey),
        body: myBody(),
      ),
    );
  }
}
