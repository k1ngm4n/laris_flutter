import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

import 'resources/app_colors.dart';

void main() => runApp(LarisPOS());

var myTheme = ThemeData(
    brightness: Brightness.dark,
    primaryColor: AppColors.PRIMARY_COLOR,
    primaryColorLight: AppColors.PRIMARY_COLOR_LIGHT,
    primaryColorDark: AppColors.PRIMARY_COLOR_DARK,
    accentColor: AppColors.ACCENT_COLOR);

class LarisPOS extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: myTheme,
      debugShowCheckedModeBanner: false,
      home: loginPage(),
    );
  }
}

class myButton extends StatelessWidget {
  String _mText = "";
  VoidCallback mOnPress;

  myButton(this._mText, this.mOnPress) {}

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: mOnPress,
      child: Text(_mText),
      shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(8)),
    );
  }
}

class loginTypeButton extends StatefulWidget {
  String mCaption;
  VoidCallback mCallBack;
  bool mIsSelected;

  loginTypeButton(this.mCaption, this.mIsSelected, this.mCallBack);

  @override
  _loginTypeButtonState createState() =>
      _loginTypeButtonState(mCaption, mIsSelected, mCallBack);
}

class _loginTypeButtonState extends State<loginTypeButton> {
  bool isSelected;
  String mCaption;
  VoidCallback mCallBack;

  _loginTypeButtonState(this.mCaption, this.isSelected, this.mCallBack);

  @override
  void initState() {}

  void _setState() {
    setState(() {
      isSelected = !isSelected;
      mCallBack();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      child: FlatButton(
        onPressed: _setState,
        child: Text(
          mCaption,
          style: TextStyle(color: isSelected ? Colors.black : Colors.white),
        ),
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(8),
            side: BorderSide(color: Colors.white)),
        color: isSelected ? AppColors.BLUE_LIGHT : AppColors.PRIMARY_COLOR,
      ),
    );
  }
}

class loginPage extends StatelessWidget {
  var buildBody = SafeArea(
    child: Stack(children: [
      Align(
        alignment: Alignment.topRight,
        child: Padding(
          padding: const EdgeInsets.only(right: 5),
          child: Text(
            "Ver:1.0.0",
            style: TextStyle(color: AppColors.WHITE_LIGHT),
          ),
        ),
      ),
      Align(
          alignment: Alignment.center,
          child: Container(
            width: 300,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(10),
                  child: Image.asset(
                    'assets/images/logo_white_login.png',
                    width: 200,
                    height: 60,
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Expanded(
                      child: loginTypeButton("OWNER", true, () => print("click owner")),
                    ),
                    SizedBox(width: 5),
                    Expanded(
                      child: loginTypeButton("STAFF", false, () => print("click staff")),
                    ),
                  ],
                ),
                SizedBox(height: 20),
                Container(
                    child: TextField(
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(bottom: 5),
                          isDense: true,
                          icon: Icon(Icons.person, size: 25),
                          hintText: "User ID"),
                    )),
                SizedBox(height: 20),
                Container(
                    child: TextField(
                      obscureText: true,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(bottom: 5),
                          isDense: true,
                          icon: Icon(Icons.lock, size: 25),
                          hintText: "Password"),
                    )),
                Container(
                  alignment: Alignment.centerRight,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Text("Forgot password?"),
                  ),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20, bottom: 20),
                    child: Text(
                      "Invalid User ID / Password adsf adsf adsf adsf asdfasd fasdf adsfasd",
                      style: TextStyle(color: Colors.red),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 10),
                  child: SizedBox(
                    height: 60,
                    width: double.infinity,
                    child: RaisedButton(
                      onPressed: () {},
                      color: AppColors.PRIMARY_COLOR_DARK,
                      child: Text("LOGIN"),
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(8)),
                    ),
                  ),
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "No account yet? ",
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.end,
                      ),
                      Text(
                        " Create One",
                        style: TextStyle(color: AppColors.YELLOW_GOLD),
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 5),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Try our Demo? ",
                        style: TextStyle(color: Colors.white),
                        textAlign: TextAlign.end,
                      ),
                      Text(
                        " Click here",
                        style: TextStyle(color: AppColors.YELLOW_GOLD),
                        textAlign: TextAlign.start,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )),
      Align(
        alignment: Alignment.bottomCenter,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 5),
          child: Text(
            "HW ID : 4asd3423423423",
            style: TextStyle(color: AppColors.YELLOW_LIGHT),
          ),
        ),
      )
    ]),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      resizeToAvoidBottomInset: false,
      body: buildBody,
    );
  }
}
