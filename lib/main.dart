import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app2/main_backup.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';

import 'resources/app_colors.dart';

//import 'WebCallDialog.dart';
import 'DataClass.dart';

void main() => runApp(LarisPOS());

var myTheme = ThemeData(
    brightness: Brightness.dark,
    primaryColor: AppColors.PRIMARY_COLOR,
    primaryColorLight: AppColors.PRIMARY_COLOR_LIGHT,
    primaryColorDark: AppColors.PRIMARY_COLOR_DARK,
    accentColor: AppColors.ACCENT_COLOR);

class LarisPOS extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: myTheme,
      debugShowCheckedModeBanner: false,
      home: loginPage(),
    );
  }
}

class mainContent extends StatefulWidget {
  @override
  _mainContentState createState() => _mainContentState();
}

class _mainContentState extends State<mainContent> {
  String mLoginType = "OWNER";
  String mErrorMessge = "";

  bool mShowLoginButton = true;

  FocusNode mFocus_Owner_ID = new FocusNode();
  FocusNode mFocus_Owner_PWD = new FocusNode();
  TextEditingController mOwner_ID =
      TextEditingController(text: "kingman.husada@gmail.com");
  TextEditingController mOwner_PWD = TextEditingController(text: "12345678");

//   Widget wOwner() {
//     return Visibility(
// //      visible: mLoginType == "OWNER" ? true : false,
//       visible: true,
// //      replacement: mStaff(),
//       child: Column(
//         children: <Widget>[
//           Container(
//               child: TextField(
//             controller: mOwner_ID,
//             focusNode: mFocus_Owner_ID,
//             decoration: InputDecoration(
//                 contentPadding: EdgeInsets.only(bottom: 5),
//                 isDense: true,
//                 icon: Icon(Icons.person, size: 25),
//                 hintText: "User ID"),
//           )),
//           SizedBox(height: 20),
//           Container(
//               child: TextField(
//             focusNode: mFocus_Owner_PWD,
//             controller: mOwner_PWD,
//             obscureText: true,
//             decoration: InputDecoration(
//                 contentPadding: EdgeInsets.only(bottom: 5),
//                 isDense: true,
//                 icon: Icon(Icons.lock, size: 25),
//                 hintText: "Password"),
//           )),
//           Container(
//             alignment: Alignment.centerRight,
//             child: Padding(
//               padding: const EdgeInsets.only(top: 10),
//               child: InkWell(
//                   child: Text("Forgot password?"),
//                   onTap: () => launch('http://larispos.com')),
//             ),
//           ),
//         ],
//       ),
//     );
//   }

  Widget wOwner() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
              child: TextField(
            controller: mOwner_ID,
            focusNode: mFocus_Owner_ID,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.only(bottom: 5),
                isDense: true,
                icon: Icon(Icons.person, size: 25),
                hintText: "User ID"),
          )),
          SizedBox(height: 20),
          Container(
              child: TextField(
            focusNode: mFocus_Owner_PWD,
            controller: mOwner_PWD,
            obscureText: true,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.only(bottom: 5),
                isDense: true,
                icon: Icon(Icons.lock, size: 25),
                hintText: "Password"),
          )),
          Container(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.only(top: 10),
              child: InkWell(
                  child: Text("Forgot password?"),
                  onTap: () => launch('http://larispos.com')),
            ),
          ),
        ],
      ),
    );
  }

  FocusNode mFocus_Staff_STORE_ID = new FocusNode();
  FocusNode mFocus_Staff_ID = new FocusNode();
  FocusNode mFocus_Staff_PWD = new FocusNode();
  TextEditingController mStaff_ID = TextEditingController();
  TextEditingController mStaff_PWD = TextEditingController();
  TextEditingController mStaff_STORE_ID = TextEditingController();

  // Widget mStaff() {
  //   return Visibility(
  //     //visible: mLoginType == "STAFF" ? true : false,
  //     visible: true,
  //     child: Column(
  //       children: <Widget>[
  //         Container(
  //             child: TextField(
  //           focusNode: mFocus_Staff_STORE_ID,
  //           controller: mStaff_STORE_ID,
  //           decoration: InputDecoration(
  //               contentPadding: EdgeInsets.only(bottom: 5),
  //               isDense: true,
  //               icon: Icon(Icons.store, size: 25),
  //               hintText: "Store ID"),
  //         )),
  //         SizedBox(height: 20),
  //         Container(
  //             child: TextField(
  //           focusNode: mFocus_Staff_ID,
  //           controller: mStaff_ID,
  //           decoration: InputDecoration(
  //               contentPadding: EdgeInsets.only(bottom: 5),
  //               isDense: true,
  //               icon: Icon(Icons.person, size: 25),
  //               hintText: "User ID"),
  //         )),
  //         SizedBox(height: 20),
  //         Container(
  //             child: TextField(
  //           focusNode: mFocus_Staff_PWD,
  //           controller: mStaff_PWD,
  //           obscureText: true,
  //           decoration: InputDecoration(
  //               contentPadding: EdgeInsets.only(bottom: 5),
  //               isDense: true,
  //               icon: Icon(Icons.lock, size: 25),
  //               hintText: "Password"),
  //         )),
  //       ],
  //     ),
  //   );
  // }

  Widget mStaff() {
    return Container(
      key: Key('STAFF'),
      child: Column(
        children: <Widget>[
          Container(
              child: TextField(
            focusNode: mFocus_Staff_STORE_ID,
            controller: mStaff_STORE_ID,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.only(bottom: 5),
                isDense: true,
                icon: Icon(Icons.store, size: 25),
                hintText: "Store ID"),
          )),
          SizedBox(height: 20),
          Container(
              child: TextField(
            focusNode: mFocus_Staff_ID,
            controller: mStaff_ID,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.only(bottom: 5),
                isDense: true,
                icon: Icon(Icons.person, size: 25),
                hintText: "User ID"),
          )),
          SizedBox(height: 20),
          Container(
              child: TextField(
            focusNode: mFocus_Staff_PWD,
            controller: mStaff_PWD,
            obscureText: true,
            decoration: InputDecoration(
                contentPadding: EdgeInsets.only(bottom: 5),
                isDense: true,
                icon: Icon(Icons.lock, size: 25),
                hintText: "Password"),
          )),
        ],
      ),
    );
  }

  // Widget mStaff2() {
  //   return Container(
  //     key: Key('first'),
  //     width: 150,
  //     height: 50,
  //     color: Colors.teal,
  //     child: Text("AAAAAAAAAAAAA"),
  //   );
  // }

  // Widget mStaff() {
  //   return Container(
  //     key: Key('first'),
  //     width: 150,
  //     height: 50,
  //     color: Colors.teal,
  //     child: const Center(
  //       child: const Text(
  //           'Woolha.com',
  //           style: const TextStyle(fontSize: 24, color: Colors.white)
  //       ),
  //     ),
  //   );
  // }

  Widget wActionSelector() {
    Widget wOwner() {
      return Expanded(
          key: Key('OWNER'),
          child: SizedBox(
            height: 40,
            child: FlatButton(
              onPressed: () {
                setState(() {
                  mLoginType = "OWNER";
                });
              },
              child: Text(
                "OWNER",
                style: TextStyle(
                    color: mLoginType == "OWNER" ? Colors.black : Colors.white),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                  side: BorderSide(color: Colors.white)),
              color: mLoginType == "OWNER"
                  ? AppColors.BLUE_LIGHT
                  : AppColors.PRIMARY_COLOR,
            ),
          ));
    }

    Widget wStaff() {
      return Expanded(
          child: SizedBox(
        height: 40,
        child: FlatButton(
          onPressed: () {
            setState(() {
              mLoginType = "STAFF";
            });
          },
          child: Text(
            "STAFF",
            style: TextStyle(
                color: mLoginType == "STAFF" ? Colors.black : Colors.white),
          ),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
              side: BorderSide(color: Colors.white)),
          color: mLoginType == "STAFF"
              ? AppColors.BLUE_LIGHT
              : AppColors.PRIMARY_COLOR,
        ),
      ));
    }

    return Column(
      children: <Widget>[
        Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            wOwner(),
            SizedBox(width: 5),
            wStaff(),
          ],
        ),
        SizedBox(height: 30),
      ],
    );
  }

  Widget wLogo() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Image.asset(
        'assets/images/logo_white_login.png',
        width: 200,
        height: 60,
      ),
    );
  }

  Widget wErrorMessage() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 20, bottom: 20),
        child: Text(
          mErrorMessge,
          style: TextStyle(color: Colors.red, fontSize: 16),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  void submitRequest() {
    bool validInput = false;
    String errorMsg = "";
    switch (mLoginType) {
      case "OWNER":
        {
          if (mOwner_ID.text == "") {
            //FocusScope.of(context).requestFocus(mFocus_Owner_ID);
            errorMsg = "User ID is empty";
          } else if (mOwner_PWD.text == "") {
            //FocusScope.of(context).requestFocus(mFocus_Owner_PWD);
            errorMsg = "Password is empty";
          } else {
            validInput = true;
          }
        }
        break;

      case "STAFF":
        {
          if (mStaff_STORE_ID.text == "") {
            //FocusScope.of(context).requestFocus(mFocus_Staff_STORE_ID);
            errorMsg = "Store ID is empty";
          } else if (mStaff_ID.text == "") {
            //FocusScope.of(context).requestFocus(mFocus_Staff_ID);
            errorMsg = "User ID is empty";
          } else if (mStaff_PWD.text == "") {
            //FocusScope.of(context).requestFocus(mFocus_Staff_PWD);
            errorMsg = "Password is empty";
          } else {
            validInput = true;
          }
        }
        break;
    }

    updateState(bool showLoginButton, String msg) {
      setState(() {
        mErrorMessge = msg;

        mShowLoginButton = showLoginButton;
      });
    }

    if (validInput) {
      updateState(false, "Processing...");

      loginWS().then((result) {
        if (result.mStatus) {
          updateState(false, "SUDAH SUKSES LOGIN NEH");
          //goto halaman login utama
        } else {
          updateState(true, result.mMessage);
        }
      }, onError: (error) {
        updateState(true, "ERROR " + error);
      });
    } else {
      updateState(true, errorMsg);
    }
  }

  void gotoMainActivity(BuildContext context, String jsonStr) {
    Navigator.of(context).push(MaterialPageRoute<Null>(
      builder: (BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            elevation: 1.0,
            title: Text("AAAAA"),
            centerTitle: true,
            backgroundColor: Colors.green,
          ),
          body: Text(jsonStr),
        );
      },
    ));
  }p

  Future _mockService(String error) {
    return new Future.delayed(Duration(milliseconds: 500), () {
      if (error != null) {
        setState(() {
          mShowLoginButton = true;
          mErrorMessge = error;
        });

        throw error;
      }
    });
  }

  Future<Result> loginWS() async {
    String url = 'https://www.larispos.com/api/auth';

    Map data = {
      'new_install': '0',
      'email': mOwner_ID.text,
      'pwd': mOwner_PWD.text,
      'get_license': '1',
      'hardware_name': 'kingman-coba',
      'hardware_screen': 'xx',
      'hardware_os': 'win10'
    };
    var mBody = json.encode(data);

    var uri = Uri.parse(url);
    final response = await http.post(uri,
        headers: {
          'SOLISCLOUD-API-KEY': 'asdfasdfasdfasdfasdfasdfasdfasdf',
          'HARDWARE-ID': '55E1-2EE1-C908-53D7',
          'APP-VER': '1_8_1_0',
          'APP-TYPE': 'ANDROID',
          'Content-type': 'application/json',
        },
        body: mBody);

    var result = Result();
    if (response.statusCode == 200) {
      result.mStatus = true;
    } else {
      result.mStatus = false;
      result.mMessage = response.body;
    }

    return result;
  }

  // Future<Result> testDialog() async {
  //   Result returnVal =
  //       await showDialog(context: context, barrierDismissible: false, builder: (BuildContext context) => myDialog());
  //
  //   return await returnVal;
  // }

  Widget wLoginButton() {
    return Visibility(
      visible: mShowLoginButton,
      replacement: Container(
          margin: EdgeInsets.only(bottom: 10),
          height: 60,
          width: 60,
          child: CircularProgressIndicator()),
      child: Container(
        margin: EdgeInsets.only(bottom: 10),
        child: SizedBox(
          width: double.infinity,
          height: 60,
          child: RaisedButton(
            onPressed: () {
              submitRequest();
            },
            color: AppColors.PRIMARY_COLOR_DARK,
            child: Text("LOGIN"),
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(8)),
          ),
        ),
      ),
    );
  }

  Widget wWebLink() {
    return Column(
      children: <Widget>[
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "No account yet? ",
                style: TextStyle(color: Colors.white),
                textAlign: TextAlign.end,
              ),
              InkWell(
                child: Text(
                  " Create One",
                  style: TextStyle(color: AppColors.YELLOW_GOLD),
                  textAlign: TextAlign.start,
                ),
                onTap: () => launch('http://larispos.com'),
              ),
            ],
          ),
        ),
        SizedBox(height: 5),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Try our Demo? ",
                style: TextStyle(color: Colors.white),
                textAlign: TextAlign.end,
              ),
              InkWell(
                child: Text(
                  " Click here",
                  style: TextStyle(color: AppColors.YELLOW_GOLD),
                  textAlign: TextAlign.start,
                ),
                onTap: () => launch('http://larispos.com'),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _renderWidget() {
    return mLoginType == "OWNER" ? wOwner() : mStaff();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          wLogo(),
          wActionSelector(),
          AnimatedSwitcher(
            duration: const Duration(milliseconds: 500),
            child: _renderWidget(),
            transitionBuilder: (Widget child, Animation<double> animation) {
              return ScaleTransition(scale: animation, child: child);
            },
          ),
          wErrorMessage(),
          wLoginButton(),
          wWebLink(),
        ],
      ),
    );
  }
}

class bodyAsStatelessWidgetClass extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(children: [
        Align(
          alignment: Alignment.topRight,
          child: Padding(
            padding: const EdgeInsets.only(right: 5),
            child: Text(
              "Ver:1.0.0",
              style: TextStyle(color: AppColors.WHITE_LIGHT),
            ),
          ),
        ),
        Align(alignment: Alignment.center, child: mainContent()),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 5),
            child: Text(
              "HW ID : 4asd3423423423",
              style: TextStyle(color: AppColors.YELLOW_LIGHT),
            ),
          ),
        )
      ]),
    );
  }
}

class loginPage extends StatelessWidget {
  Widget bodyAsLocalWidgetFunction() {
    return SafeArea(
      child: Stack(children: [
        Align(
          alignment: Alignment.topRight,
          child: Padding(
            padding: const EdgeInsets.only(right: 5),
            child: Text(
              "Ver:1.0.0",
              style: TextStyle(color: AppColors.WHITE_LIGHT),
            ),
          ),
        ),
        Align(alignment: Alignment.center, child: mainContent()),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 5),
            child: Text(
              "HW ID : 4asd3423423423",
              style: TextStyle(color: AppColors.YELLOW_LIGHT),
            ),
          ),
        )
      ]),
    );
  }

  var bodyAsLocalVar = SafeArea(
    child: Stack(children: [
      Align(
        alignment: Alignment.topRight,
        child: Padding(
          padding: const EdgeInsets.only(right: 5),
          child: Text(
            "Ver:1.0.0",
            style: TextStyle(color: AppColors.WHITE_LIGHT),
          ),
        ),
      ),
      Align(alignment: Alignment.center, child: mainContent()),
      Align(
        alignment: Alignment.bottomCenter,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 5),
          child: Text(
            "HW ID : 4asd3423423423",
            style: TextStyle(color: AppColors.YELLOW_LIGHT),
          ),
        ),
      )
    ]),
  );

  Widget bodyAsLocalWidgetVariable = SafeArea(
    child: Stack(children: [
      Align(
        alignment: Alignment.topRight,
        child: Padding(
          padding: const EdgeInsets.only(right: 5),
          child: Text(
            "Ver:1.0.0",
            style: TextStyle(color: AppColors.WHITE_LIGHT),
          ),
        ),
      ),
      Align(alignment: Alignment.center, child: mainContent()),
      Align(
        alignment: Alignment.bottomCenter,
        child: Padding(
          padding: const EdgeInsets.only(bottom: 5),
          child: Text(
            "HW ID : 4asd3423423423",
            style: TextStyle(color: AppColors.YELLOW_LIGHT),
          ),
        ),
      )
    ]),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      resizeToAvoidBottomInset: false,
      body: bodyAsStatelessWidgetClass(),
    );
  }
}
